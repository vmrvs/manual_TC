\section{Política y Derecho}

\subsection{Política y conocimiento}

El objeto del Derecho constitucional es la política. Esta frase condensa los
aspectos fundamentales de esta disciplina, pero también revela todas sus
complejidades. No es exagerado señalar, que uno de los sellos distintivos de la
especie humana es la vida en comunidad. Si bien es cierto, otras especies del
reino animal son capaces de organizarse colectivamente, ninguna otra ha logrado
alcanzar un nivel de complejidad, ni por lejos, similar al nuestro. Desde
tiempos inmemoriales las sociedades humanas han intentado convivir de forma
organizada, a veces con mayor o menor éxito. Autores clásicos como Aristóteles
ya hicieron de esta circunstancia el punto de inicio de su reflexión política,
por ejemplo, defendiendo la tesis de que el hombre es un ser político por
naturaleza, no siendo posible su vida fuera de la comunidad. Aun cuando, podemos
no concordar del todo con el pensador griego, es innegable que la política
incide en nuestras vidas de manera muy importante y es inconcebible el mundo tal
como lo conocemos sin ella.

Como se puede intuir, el Derecho constitucional es indispensable para entender
el sistema jurídico en su conjunto, pues su objeto versa sobre un elemento que
acompaña al ser humano independientemente del lugar o época en que viva y de
cualquier otra circunstancia accidental: la necesidad de organizarse y progresar
en comunidad. Dado que el Derecho constitucional se preocupa acerca de cómo la
organización de la actividad política se traduce en normas jurídicas, es
importante comenzar su estudio con una asignatura que examine la relación
dialéctica que se produce entre Derecho y política. Este es, precisamente, el
sentido de una asignatura de teoría constitucional.

En la medida que la política posee un carácter verdaderamente esencial en la
vida de las personas, ésta ha sido objeto de reflexión y conocimiento desde la
antigüedad y de ella se han ocupado varias disciplinas. Las dos principales han
sido la teoría y la filosofía política, las que, a su vez, son importante
insumos para la teoría constitucional.  

En cierto sentido, la teoría constitucional es una tecnología que permite
aplicar los conocimientos derivados de la teoría política y de la filosofía
política. La RAE define tecnología, como el conjunto de teorías y de técnicas
que permiten el aprovechamiento práctico del conocimiento científico. Las
tecnologías permiten a los seres humanos transformar el mundo para satisfacer
nuestras necesidades.  

La teoría política es un área del conocimiento bajo cuyo alero se han aglutinado
diversas preocupaciones intelectuales, que poseen como punto en común el
análisis de las ideas sobre las que se asienta la organización de la convivencia
en sociedad. Desde esta perspectiva, preguntas tales como qué es el Estado o qué
es totalitarismo político, son interrogantes clásicamente planteadas desde la
vereda de la teoría política. Por lo mismo, se puede señalar que la función de
la teoría política es la comprensión de las condiciones bajo las cuales tienen
lugar la acción y el comportamiento político. Por otra parte, la filosofía
política se preocupa del mismo objeto de estudio, pero con un enfoque
eminentemente práctico, es decir, constituye un saber para la acción. Según
Valeria Nurock la filosofía política posee cuatro funciones: la práctica, de
orientación, de reconciliación y la utópica. Todas ellas buscan la construcción
de un conocimiento que busca influir en su objeto y transformarlo en su mejor
versión posible. 

Ambas disciplinas si bien hoy en día se conciben como saberes diferentes poseen
una larga historia común, y en cualquier caso, mantienen vínculos de
complementariedad que se extienden hasta la actualidad. Además de poseer
idéntico objeto de estudio, las particularidades de este mismo hacen que en
ocasiones sea difícil evitar la tensión entre gnosis y praxis. Por una parte, el
mundo de lo político es una construcción social que no existe más que en las
ideas que la informan, ideas que por cierto están cargadas de las circunstancias
que afectan a quienes las sustentan. Incluso más, el pensamiento postmoderno ha
negado sistemáticamente que se puede trazar una distinción, entre teoría y
práctica en cuestiones sociales, considerando que una teoría es una caja de
herramientas para la acción, para ellos una teoría ``no expresará, no traducirá,
no aplicará una práctica, es una práctica''. 

Pues bien, la teoría constitucional es una síntesis entre ambos saberes. Busca
conocer, pero también corregir, es al mismo tiempo un cognoscere y un agere.
Esto permite entender que, la teoría constitucional a veces busca describir el
mundo de las instituciones políticas, pero a veces también lo critica intentando
influir sobre él. 

\subsection{¿Qué es política?}

No es fácil definir qué es la política, ni menos cuál es la mejor forma de
ponerla en práctica. La verdad es que cualquier intento en este sentido será
parcial. Sólo a título ejemplar, podemos mencionar que uno de los intentos más
célebres de desentrañar este concepto, se puede encontrar en el libro de Hannah
Arendt titulado ``La Promesa de la Política''.

Si se pudiera resumir en unas pocas ideas su propuesta, la pregunta que motiva a
la reflexión sería, parafraseando la célebre pregunta de Heidegger, ¿por qué
existe alguien y no más bien nadie?, asumiendo en consecuencia una tesis
aparentemente sencilla: ``la política trata del estar juntos, los unos con los
otros, a pesar del inexorable hecho de la pluralidad entre los seres humanos''.
La cuestión de suyo no es baladí, menos en tiempos como los actuales, donde la
violencia amenaza constantemente la existencia de nuestro mundo. Tal como
sucedió en los momentos más álgidos del siglo XX, es importante la reflexión
acerca de qué es lo que hace posible la existencia de lo público y por qué es
necesario conservar el ágora como espacio de encuentro y desencuentro. ``A la
pregunta por el sentido de la política, hay una respuesta tan sencilla y tan
concluyente en sí misma que se diría que otras respuestas están totalmente de
más. La respuesta es: el sentido de la política es la libertad'', dirá Arendt.

El libro, como toda su obra es una defensa de la política, a pesar del prejuicio
en contra de ella. ¿No es acaso la política la responsable de las catástrofes
más terribles del pasado siglo? Si ello es así, ¿por qué mejor no sustituirla
por un sistema meramente burocrático, o en palabras de Marx, por la ``mera
administración de las cosa''? La respuesta de Arendt será un rotundo no y para
ello la teórica alemana se hace cargo de los prejuicios que frecuentemente se
suelen levantar en contra de la actividad política. Expresiones cotidianas, que
todos ---incluyéndonos a nosotros mismos--- hemos proferido alguna vez, son
brillantemente contrarrestadas por A\-rendt, quien se resiste a la idea de un
mundo (político) en ruinas. En efecto, la trampa del totalitarismo, justamente,
es tratar de explicar todo el mundo a partir de unos pocos prejuicios, a través
de pseudoteorías que pretenden abarcar toda la realidad social y política.

Con esto Arendt también cuestiona fuertemente el programa político del
capitalismo, donde la política siempre es un medio para alcanzar algún otro fin,
aserción que implícitamente contiene un desprecio hacia ella. En un contexto
donde prima lo material por respecto de lo intangible, el individuo por sobre la
comunidad, la eficiencia y la capacidad productiva como valores supremos, la
política pierde sentido, pues esta no pasa de ser un medio para la satisfacción
de intereses egoístas. En consecuencia, en el mejor de los casos esta resulta un
sucedáneo de la guerra, que permite que resolvamos nuestras diferencias de
manera relativamente pacífica. Según la autora, aquella mirada es incorrecta,
porque la verdadera esencia de la política consiste en que esta es un fin en sí
misma: ``ser libre y vivir en una Polis son la misma cosa''. La política se
relaciona con uno de los aspectos esenciales de la condición humana: la
posibilidad de construir y reconstruir el mundo, es decir, el conjunto de
categorías a partir de las cuales entendemos y damos sentido a nuestra
existencia. Es por esta razón que la política está íntimamente vinculada a la
posibilidad de hablar y ser escuchado, lo que, en otros términos, se reconduce a
la posibilidad de un diálogo entre iguales. Es decir, esta libertad de palabra
(que modernamente llamaremos libertad de expresión), tiene como base el hecho de
que entender y dar forma al mundo solo es posible entre muchos, en la medida que
todos aquellos que son considerados iguales intercambien sus perspectivas.

Es por todo esto que la violencia solo es marginalmente política, quizás como un
último recurso para mantener la Polis frente a una amenaza, cuando de hecho la
política ya no es posible. Esta concepción del ser humano, como un individuo que
vive en comunidad, se vio derechamente afectada luego de la Revolución
Industrial. En este sentido, la autora afirma, casi con desconsuelo: ``Tras la
Revolución Industrial para el ser humano la experiencia de la fabricación
alcanzó una predominancia tan insuperable que las incertidumbres de la acción
pudieron ser olvidadas por completo; entonces se pudo comenzar a hablar acerca
de `hacer el futuro' y de `construir y mejorar la sociedad', como si se
estuviese hablando de hacer sillas y de construir y mejorar las casas''. 

No hay que perder de vista que esta reflexión surge en un contexto de extrema
crueldad como fue el régimen nazi, donde Arendt en su calidad de pensadora
judía, vivió en carne propia los horrores del III Reich. La cita de esta autora
no es casual, puesto que su trabajo se centró en el estudio del totalitarismo,
el tipo de gobierno que le tocó combatir, y que precisamente, se sitúa en las
antípodas del constitucionalismo. Si se piensa en términos metafóricos, la
teoría constitucional es un pozo acumulado de conocimientos que es necesario
conservar y alimentar. Precisamente, en Los Orígenes del Totalitarismo, una de
sus obras más célebres, plantea que los totalitarismos del siglo XX, en modo
alguno, guardaban relación con las grandes tradiciones de pensamiento político y
filosófico de Occidente: habían surgido, precisamente, allí donde la tradición
se había roto. 

Del mismo modo, una de las estrategias de los gobiernos tiránicos fue la de
reducir a los seres humanos a la condición propia del animal laborans, en la
cual la igualdad no existe y la política no es posible. Ello se consigue a
través de la disolución de los lazos entre las personas. Dirá Arendt, en este
sentido, que: ``los Gobiernos totalitarios, como todas las tiranías, no podrían
ciertamente existir sin destruir el terreno público de la vida, es decir, sin
destruir, aislando a los hombres, sus capacidades políticas. Pero la dominación
totalitaria como forma de gobierno resulta nueva en cuanto que no se contenta
con este aislamiento y destruye también la vida privada. Se basa ella misma en
la soledad, en la experiencia de no pertenecer en absoluto al mundo, que figura
entre las experiencias más radicales y desesperadas del hombre''. He ahí la
novedad de las fórmulas totalitarias que aparecieron en el siglo XX. En el
Gobierno constitucional las leyes positivas están concebidas para erigir
fronteras y establecer canales de comunicación entre las personas. Por el
contrario, presionando a los hombres unos contra otros, el terror total destruye
el espacio entre ellos.

\subsection{Teoría y teorías de la Constitución}

Antes se ha dicho que la teoría de la Constitución pretende explicar la realidad
constitucional, pero al mismo tiempo, busca influir sobre ella. Sin embargo, no
existe una teoría constitucional ortodoxa que se funde en premisas que sean
incontrovertidas.  En cierto sentido, todos los conceptos de la teoría y la
filosofía política son conceptos esencialmente controvertidos en el sentido que
propuso Gallie. De este modo, la teoría constitucional es altamente dependiente
de una metateoría, que normalmente ensambla argumentos descriptivos y
valorativos. En la medida de que estas premisas son normalmente debatidas entre
los autores, ello produce como resultado que en la práctica existan distintas
teorías de la Constitución, que más adelante conoceremos con algo más de
detalle. 

Por ahora y solo a título ejemplar, las tesis de Hannah Arendt que antes se
explicaron, se enmarcan en la tradición de pensamiento denominada republicanismo
la que, en términos generales, se caracteriza por una alta valoración de la vida
en comunidad, cuyo paradigma central es la democracia. Para estos autores, el
modelo de sociedad más justo es aquel en el que las personas son capaces de
forjar lazos de cooperación y de resolver a través de la deliberación racional
los problemas de la vida en común. Otro ilustre precedente del republicanismo lo
encontramos en Jean Jacques Rousseau, quien defendió los postulados de la
igualdad de participación política en la creación de las leyes, como forma de
vencer la opresión del Estado absoluto. Todos esto lleva a la necesaria
conclusión de que, si suscribimos esta visión optimista acerca de la política,
el Derecho constitucional sería su mejor aliado, a través de la creación de
procedimientos e instituciones que permitan que los órganos del Estado reflejen
en su composición la pluralidad de visiones existentes en una sociedad con la
mayor fidelidad posible. 

Probablemente, la teoría de la Constitución que históricamente ha generado mayor
adhesión es la teoría liberal, la que centra todo su interés en el individuo y
sus derechos fundamentales y concibe a la Constitución como un instrumento de
limitación del poder político. Esta parte de premisas diferentes respecto de la
anterior: la desconfianza frente a la política. Si se piensa bien, los autores
liberales cuentan a su favor con mucha evidencia empírica de que el Estado ha
sido una maquinaria de guerra y exterminio, y en efecto, lo sigue siendo. Esto
ha llevado a que la teoría liberal se haya desarrollado, desde siempre, como un
intento de establecer mecanismos de limitación frente al poder del Estado. Por
esta razón, el Derecho constitucional liberal refleja estructuralmente la
tensión entre poder y regla, estructurando un conjunto de instituciones que no
se preocupan tanto por la posición que las personas tienen dentro de estas, sino
más bien su objetivo central es construir y delimitar ámbitos de inviolabilidad
frente a la actividad estatal. 

Tanto los republicanos como los liberales, cada uno a su manera, reaccionan
frente un hecho prácticamente innegable, que es el ser humano puede cooperar
entre sí, pero también puede ejercer la violencia de forma brutal contra sus
semejantes. Ambas corrientes, cada una a su manera, intentan diseñar un sistema
jurídico que excluya a la violencia de las decisiones e interacciones
colectivas, o que al menos, la utilice como último recurso. No obstante, nuestra
realidad nos muestra diariamente, a veces de forma muy elocuente, que la
violencia y el uso de la fuerza también forman parte esencial de lo que nos hace
humanos. La prueba más evidente de ello es que el Derecho, en cualquiera de sus
manifestaciones, es en definitiva coacción. Esto ha llevado que determinadas
escuelas de pensamiento político no rechacen de antemano el uso de la fuerza,
sino que, por el contrario, lo conviertan en un instrumento completamente
válido, si es útil para alcanzar determinados objetivos. Llamaremos a estas
tesis, a falta de un nombre mejor, constitucionalismo autoritario. Si bien en
esta expresión podemos englobar visiones muy diferentes sobre proceso político,
todas ellas se caracterizan por resaltar la importancia el uso de la fuerza como
método de acción política y de resolución de conflictos. El autoritarismo es tan
importante en la construcción del Derecho moderno, que la primera teoría acerca
del Estado moderno, la de Thomas Hobbes, ha determinado de manera muy importante
la evolución del constitucionalismo en los siguientes 300 años. Sin perjuicio de
que volveremos sobre este tema en la sección siguiente, para Hobbes el fin más
importante del Estado es la paz social y el orden, la que debe ser alcanzada a
cualquier precio, incluso ejerciendo violencia en contra de los súbditos. Para
esta visión, el Derecho constitucional es un instrumento que permite alcanzar un
determinado orden social y ejecutar la decisión del soberano. Y aunque a primera
vista esta forma de entender la Constitución nos puede parecer contraintuitiva,
la verdad es que ni liberales ni republicanos han podido o han querido
deshacerse de ella en su totalidad.

Este mero ejercicio de reflexión inicial, sin ningún ánimo de exhaustividad, ha
tenido por solo por objeto relevar cómo el Derecho constitucional está
íntimamente ligado a la política, a la manera cómo se la entienda y a la actitud
que se tenga respecto de ella. Como la política es un concepto controvertido, al
punto de que la teoría y la filosofía política no han logrado ponerse de acuerdo
acerca de cuál de estas visiones debe prevalecer, lo propio ha sucedido en el
plano jurídico. Esta situación tiene un impacto directo en nuestra disciplina,
puesto que en muchos de los debates que existen dentro de ella, se refleja toda
en toda su magnitud la tensión conceptual y filosófica en torno al fenómeno
político. Por ejemplo, ¿debemos mantener a los terroristas todas las garantías
del debido proceso, aún cuando perdamos capacidad de prevenir atentados
mortales, o debemos aplicar en esos casos normativa de excepción?, o ¿es
legítimo que un tribunal constitucional revoque las decisiones de la mayoría
parlamentaria democráticamente electa?, son todas estas cuestiones que no pueden
resolverse sin una teoría filosófica de base.

En síntesis, lo que por ahora es necesario enfatizar, es que el Derecho
constitucional se encuentra vertebrado por diferentes tradiciones de
pensamiento, que a veces tienden a entremezclarse, pero que en otras ocasiones
directamente se enfrentan dando lugar a respuestas contradictorias.  Sin tener
en cuenta esta circunstancia, es imposible entender la arquitectura
constitucional contemporánea, su acervo teórico, sus métodos de razonamiento y
sus relaciones con el resto del ordenamiento jurídico.

