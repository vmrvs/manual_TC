\section{Los sistemas electorales}

\subsection{Los sistemas electorales: concepto, clasificación y principales 
fórmulas}

Un sistema electoral es un conjunto de medios a través de los cuales la voluntad de los ciudadanos se transforma en órganos de gobierno o de representación política. En este sentido, se puede decir que los sistemas electorales son procedimientos para transformar votos en escaños.

Sin perjuicio de que la regla de oro de la democracia es que accede al cargo quien obtiene más votos (principio First Past the Post o FPP), no siempre es fácil poner en práctica esta idea, dada la composición compleja de los órganos políticos. En efecto, dicho sistema solo funcionaría de forma correcta en el caso de órganos unipersonales que ejerzan sus atribuciones en el marco de todo el Estado (por ejemplo, el Presidente de la República en el sistema presidencial), pues en cualquier otro caso es necesario dar respuesta a una serie de interrogantes, cómo por ejemplo: ¿quiénes pueden ser votados?; ¿de cuántos votos dispone cada elector?; ¿cuántos representantes se eligen en cada demarcación electoral?; ¿cómo se determinan y delimitan los distritos y secciones electorales?; ¿cómo deben emitirse y contarse los sufragios?, etc. 

Por otra parte, ni siquiera para el caso de órganos unipersonales es posible implementar un sistema del tipo FPP, dado que existen otros valores que los sistemas electorales cautelan, como, por ejemplo, la estabilidad y la gobernabilidad. De este modo, optar por un sistema electoral concreto es una de las decisiones institucionales más importantes en una democracia representativa, pues generalmente, estos influyen notablemente en la composición del parlamento y la correlación de fuerzas entre los diferentes grupos parlamentarios.

\subsection{Elementos de los sisemas electorales}

En términos generales, se componen de tres elementos: circunscripción, estructura del voto y fórmula electoral.

\subsubsection{Circunscripción}

Son cada una de las divisiones territoriales en que se organiza la elección de representantes. La circunscripción funciona como unidad de imputación de los escaños, esto significa que a cada una de éstas se le atribuye un cierto número de ellos. 

Se habla de tamaño de la circunscripción para hacer referencia al número de escaños que se elige en cada circunscripción, de modo tal, que es posible distinguir entre circunscripciones uninominales (se elige un solo cargo) y circunscripciones plurinominales (se eligen varios cargos). Un caso sui generis fue el sistema electoral binominal, aun parcialmente vigente en Chile para las elecciones parlamentarias, en el que las circunscripciones son binominales (se eligen dos cargos por circunscripción)\footnote{La Ley N° 20.840, de 05 de mayo de 2015, que modifica la Ley Orgánica Constitucional de Votaciones Populares y Escrutinios, sustituyó el sistema electoral binominal por uno de naturaleza proporcional.}.  

La operación de construcción de las circunscripciones recibe el nombre de distritación, y en ella se utilizan diversos criterios, por ejemplo, se pueden hacer servir unidades político-administrativas ya existentes o subdivisiones de éstas, se pueden crear unidades nuevas buscando que haya un equilibrio entre el número de electores de todas ellas, o incluso, se pueden mezclar estos dos criterios. También es muy usual que en el caso de órganos bicamerales una de las cámaras se elija sobre la base de un tipo de circunscripción y la otra sobre un sistema totalmente distinto. Un buen supuesto para ilustrar esta situación está dado por las elecciones parlamentarias en EEUU, donde para elegir al Senado se utiliza el primer criterio, o sea, cada estado de la federación elige dos cargos, en cambio para la Cámara de Representantes se elige un número variable dependiendo de la población de cada estado\footnote{Así por ejemplo California, que es el estado más poblado, elige 53 Representantes mientras que Alaska, el Estado menos poblado elige solamente uno.}. 

Los diseñadores de distritos tienen en cuenta dos grandes principios cuando van a trazar límites, con la población vista desde un mapa:

\begin{description}

\item[Compactaciín:] el distrito debe ser razonablemente compacto desde el punto de vista geográfico, así por ejemplo, el distrito debería ser una sola pieza interconectada y no fragmentada, de forma tal de que todo su territorio sea contiguo.

\item[Cohesión:] la atención se pondría en las comunidades de interés, sobre todo respectando cuestiones históricas y culturales.

\end{description}

Es acertado decir que la delimitación se vuelve un problema mayor, cuanto más pequeño sea el número de miembros elegidos en cada distrito. De ahí que no sea extraño que la delimitación de distritos uninominales sea la técnica más politizada. En la literatura especializada el tema de la construcción de las circunscripciones ha sido latamente analizado. Uno de los aspectos que ha recibido mayor atención es el de la manipulación interesada de circunscripciones, supuesto conocido como gerrymandering, que es una práctica que consiste en una alteración de las circunscripciones electorales de un territorio, uniéndolas, dividiéndolas o asociándolas, con el objeto de mejorar o empeorar los resultados de una determinada fuerza política. Se puede hacer también, por ejemplo, a través de atribuir mayor representación a distritos con menor población.

En el caso chileno han sido varias las acusaciones de gerrymandering que se han vertido sobre el sistema electoral, dado que no siempre se utiliza el mismo criterio territorial para construir los distritos electorales, así por ejemplo, en algunas Regiones se suele dividir la respectiva Región entre circunscripción costa y cordillera, mientras en otras, se hace lo propio entre circunscripción sur y norte\footnote{El origen del término está en esta caricatura política elaborado por Gilbert Stuart en 1812. El nombre de la caricatura es The Gerrymander, juego de palabras con el apellido del gobernador de Massachusetts, Elbridge Gerry y salamander (salamandra en inglés), por la caprichosa forma del distrito electoral, creado por éste.}.

\subsubsection{Estructura del voto}

Es la forma establecida para que el elector exprese su voluntad política. En Derecho comparado existen múltiples sistemas, y en general, su configuración depende de la imaginación de quienes participen en el diseño institucional. La distinción más clásica consiste en diferenciar entre voto único y voto múltiple, según el número de opciones que puede seleccionar el elector. Dentro de las categorías de voto múltiple, las opciones también son también de lo más variadas: así por ejemplo, se puede otorgar al ciudadano tantos votos como escaños a elegir o un número menor de ellos, o incluso se puede establecer algún sistema mixto, como por ejemplo, el voto alternativo1 o el voto único transferible2. Asimismo, también es posible distinguir en cuanto a la estructura de voto, entre sistemas de votos a título personal y de representación por lista. En el primero, los escaños se atribuyen a los candidatos, en cambio en el segundo, aunque suene tautológico, los escaños se atribuyen a las listas. En este último caso, es posible subdistinguir entre sistemas de lista abierta y sistemas de lista cerrada. El criterio determinante para diferenciar entre unas y otras, está dado en términos de la facultad que le conceden al elector para votar por el candidato o por el partido de su preferencia. En las listas cerradas el orden de los candidatos es determinado por los partidos y los electores no pueden expresar su preferencia por alguno de ellos. En el caso de las listas abiertas, los votantes pueden indicar no sólo su partido, sino también su candidato favorito dentro de ese partido, esto es importante, pues el orden en que los candidatos son electos está determinado por el número de votos individuales que reciben.

\subsubsection{Fórmula electoral}

La fórmula electoral es el conjunto de operaciones matemáticas que permiten traducir los votos en escaños. Las fórmulas electorales en ningún caso son neutras, sino que claramente están creadas para que el sistema tienda a producir unos determinados resultados. En este sentido, se suele distinguir, grosso modo, entre fórmulas electorales mayoritarias y minoritarias. Desde luego, también existe la posibilidad de mezclar fórmulas de distinta naturaleza para morigerar los efectos de estas, tal es el caso de la elección de los miembros del Bundestag en Alemania3. A continuación, realizaremos un análisis más pormenorizado de los principales sistemas electorales existentes en el Derecho comparado, teniendo en cuenta la interacción entre cada uno de sus elementos. 

\subsection{Clasificación de los sistemas electorales}

\subsubsection{Mayoritarios}

Los sistemas mayoritarios tienen por objeto dotar a un régimen político de estabilidad y gobernabilidad. Con esta finalidad, su principal propósito es que quien ejerce la dirección de los órganos del Estado pueda contar con la mayoría necesaria para llevar a cabo su programa político sin dificultades, y sin que la oposición pueda interferir obstaculizando los procesos deliberativos de adopción de decisiones políticas. De este modo, el sistema tiende a sobrerrepresentar a la mayoría, y al mismo tiempo, castigar severamente a la minoría. Un buen ejemplo que ilustra esta situación, es el caso del sistema utilizado en Inglaterra para la elección parlamentaria de la Cámara de los Comunes, que se denomina sistema mayoritario uninominal, en el cual todo el país se divide en 650 circunscripciones, eligiendo cada una de ellas un cargo, de forma tal, que la lista ganadora en cada circunscripción obtiene la totalidad de la representación política y las restantes fuerzas políticas quedan sin representación parlamentaria en ese distrito. 

Otro ejemplo de sistema mayoritario es el sistema mayoritario plurinominal, el que para que produzca el efecto deseado debe ser necesariamente configurado como un sistema de lista cerrada. Así sucede en el sistema de elección del Senado en España, en este último caso, cada comunidad autónoma elige un número determinado de Senadores y la lista que obtiene más votos se lleva todos los cargos. 

Como decíamos antes, la principal ventaja de los sistemas mayoritarios está dada por su tendencia a generar fuerzas políticas poderosas y cohesionadas, lo que indudablemente, contribuye a robustecer al partido o coalición gobernante. Sin embargo, su desventaja más clara es que tienden a perjudicar a los partidos minoritarios, resultando estos normalmente excluidos del parlamento. Otro efecto normalmente descrito en la literatura es la propensión al bipartidismo, pues lo usual es que o los partidos pequeños desaparezcan o se integren a una coalición más amplia en la que pierden influencia, por lo que finalmente la lucha política se termina dando entre las dos principales fuerzas políticas. 

\subsubsection{Proporcionales}

Los sistemas proporcionales se encuentran en las antípodas de los mayoritarios, es decir, las ventajas de estos últimos son las ventajas de los primeros y viceversa. En efecto, los sistemas proporcionales buscan que el Congreso pueda integrar de la mejor forma posible a todas las fuerzas políticas que existen en la sociedad, de forma tal que la función del Parlamento sea la de reflejar el debate público en toda su riqueza y diversidad. Por el contrario, el principal problema descrito en la literatura es que los sistemas proporcionales tienden a la inestabilidad y a la atomización del sistema de partidos. Por esta razón, a pesar de que el sistema proporcional es un valioso insumo para la democracia representativa, pues contribuye a generar un nexo más directo entre el elector y el representante, es necesario diseñarlos cuidadosamente a efectos de minimizar sus consecuencias adversas.
 
Como decíamos antes, la fórmula matemática en el caso de los sistemas mayoritarios es meramente agregativa, es decir consiste en sumar votos e imputarlos a los escaños en función de la modalidad que se establece para expresar el sufragio, por el contrario, para el caso de los sistemas proporcionales la operación matemática típica consiste en obtener un cociente o cifra repartidora, según sea el caso. 

En Derecho comparado existen distintos sistemas proporcionales, los más utilizados son el sistema D'Hondt, y el método Webster, también conocido como Sainte–Lagüe. La diferencia entre estos dos ejemplos está dada por que el primero de estos sistemas busca dentro de su proporcionalidad favorecer a los partidos mayoritarios y el segundo pone énfasis en los partidos pequeños. Para entender la lógica de los sistemas proporcionales, nosotros nos detendremos exclusivamente en el sistema D'Hondt, ya que a partir del año 2017 se utiliza en nuestro país para las elecciones parlamentarias, luego de la última reforma a la Ley N° 18.700 sobre votaciones Populares y Escrutinios, y es el sistema que también se utiliza para las elecciones regionales y municipales. 

En términos generales, una vez escrutados todos los votos, la primera operación matemática que se debe realizar es calcular una serie de divisores para cada lista. La fórmula de los divisores es V/N, donde V representa el número total de votos recibidos por la lista, y N representa cada uno de los números enteros desde 1 hasta el número de cargos electos de la circunscripción objeto de escrutinio. Una vez realizadas las divisiones de los votos de cada candidatura, por cada uno de los divisores desde 1 hasta N, la asignación de cargos electos se hace ordenando los cocientes de las divisiones de mayor a menor y asignando a cada uno un escaño hasta que estos se agoten. El sistema D'Hondt, como todo sistema proporcional, es un sistema de lista y funciona sobre la base de circunscripciones plurinominales. A mayor tamaño de la circunscripción se produce un mayor efecto de proporcionalidad. Para dotar al sistema de mayor estabilidad, normalmente se suele introducir un umbral de representación, es decir, un mínimo nivel de votación que requiere una lista para garantizar su viabilidad política, el cual puede ser fijado a través de un porcentaje de los votos válidamente emitidos o través de una cantidad fija.

Probablemente, un ejemplo puede contribuir a una mejor comprensión del conjunto de operaciones que implica la aplicación de la fórmula electoral. Como ya explicábamos, en primer lugar, se ordenan las listas que superan el umbral de representación en función del número de votos obtenidos y se dividen sucesivamente por el número de escaños. El resultado es una matriz dentro de la cual se seleccionan las cifras más altas en un número equivalente al total de los escaños, cada lista que obtiene una de esas cifras obtiene un escaño. 

En un ejemplo ficticio y tremendamente simplificado, para una circunscripción de 5 escaños, el escrutinio de votos arroja los siguientes resultados: 

\begin{enumerate}[a.]

	\item Un total de 100.000 votos válidamente emitidos. 

	\item Los votos obtenidos por cada una de las listas son los siguientes:

	\begin{enumerate}[i.]

		\item Lista A: 40.000 votos

		\item Lista B: 20.000 votos

		\item Lista C: 10.000 votos

		\item Lista D: 5.000 votos

		\item Lista E: 25.000 votos

	\end{enumerate}

	\item Umbral de representación de 10.000 votos

\end{enumerate}

Entonces, el primer paso es ordenar las votaciones de mayor a menor como se puede apreciar en el cuadro \ref{tabla1}.

\begin{table}[]
\centering

\begin{tabular}{lllll}

\hline

Lista A & Lista E & Lista B & Lista C & Lista D     \\ \hline

40.000  & 25.000  & 20.000  & 10.000  & 5.000 \\ \hline

\end{tabular}
\caption{Cantidad total de votos totales por lista.}
\label{tabla1}

\end{table}

La primera operación que se debe realizar es excluir a las listas que no cumplen con el umbral mínimo de representación, en este caso el partido D, resultado expuesto en el cuadro \ref{tabla2}

\begin{table}[]
\centering

\begin{tabular}{llll}

\hline

Lista A      & Lista E      & Lista B      & Lista C      \\ \hline

40.000 & 25.000 & 20.000 & 10.000 \\ \hline

\end{tabular}
\caption{Cantidad total de votos por listas que satisfacen el umbral mínimo de representación.}
\label{tabla2}

\end{table}

El número de escaños funciona a modo de cifra repartidora, debiendo dividirse cada una de las votaciones obtenidas por las listas de forma sucesiva, tantas veces como lo exija dicha cifra. El producto de esta operación se puede graficar en la matriz que se aprecia el en cuadro \ref{tabla3}.

\begin{table}[]
\centering

\begin{tabular}{@{}llll@{}}

\toprule
Lista A     & Lista E     & Lista B     & Lista C \\ \midrule

40.000 / 1  & 25.000 / 1  & 20.000 / 1  & 10.000 / 1  \\

40.000 / 2  & 25.000 / 2  & 20.000 / 2  & 10.000 / 2  \\

40.000 / 3  & 25.000 / 3  & 20.000 / 3  & 10.000 / 3  \\

40.000 / 4  & 25.000 / 4  & 20.000 / 4  & 10.000 / 4  \\

40.000 / 5  & 25.000 / 5  & 20.000 / 5  & 10.000 / 5  \\ \bottomrule

\end{tabular}
\caption{División para calcular la cifra repartidora a partir del total de votos y cantidad de listas.}
\label{tabla3}

\end{table}

De esa matriz se deben seleccionar, finalmente, las cifras más altas hasta completar el número de escaños que aparecen resaltados en el cuadro \ref{tabla4} de la página \pageref{tabla4}. 


\begin{table}[]
\centering

\begin{tabular}{@{}llll@{}}

\toprule

Lista A                          & Lista E                        & Lista B                        & Lista C   \\ \midrule

\cellcolor[HTML]{C0C0C0}40.000   & \cellcolor[HTML]{C0C0C0}25.000 & \cellcolor[HTML]{C0C0C0}20.000 & 10.000  \\

\cellcolor[HTML]{C0C0C0}20.000   & 12.500                         & 10.000                         & 5.000   \\

\cellcolor[HTML]{C0C0C0}13.000,3 & 8.333,3                        & 6.666,6                        & 3.333,3 \\

10.000                           & 6.250                          & 5.000                          & 2.500   \\

8.000                            & 5.000                          & 4.000                          & 2.000   \\ \bottomrule

\end{tabular}
\caption{Producto de la cifra repartidora y selección de escaños.}
\label{tabla4}

\end{table}


Entonces, como resultado de este ejemplo ficticio, con los datos proporcionados, la lista A obtendría tres escaños y las listas B y E, un escaño cada una, respetivamente. 

Aunque no se puede apreciar con total claridad en este ejemplo simple, el sistema D'Hondt dentro de su proporcionalidad, tiende a beneficiar a las listas más votadas. Probablemente, si aplicáramos una fórmula que tiende a atribuir el diferencial a los partidos más pequeños como, por ejemplo, el método Webster, con total seguridad la lista A perdería un escaño en favor de la lista B o E\footnote{La fórmula para el cociente es la siguiente: $V/2S+1$, donde $V$ es el número total de votos que la lista recibió y S es el número de asientos que esta ha obtenido hasta ese momento. Inicialmente, $S$ es cero para todas las listas.}.

\subsection{El sistema electoral chileno: análisis y crítica}

Uno de los aspectos más complejos de la democracia chilena ha sido el sistema electoral. 

\subsubsection{Sistema electoral para las elecciones presidenciales}

Para el caso del Presidente de la República, el sistema electoral establecido por la Constitución de 1980 es un sistema mayoritario a dos vueltas sobre la base de una única circunscripción. Esto significa, que para que uno de los candidatos sea electo Presidente de la Republica requiere de la mayoría absoluta de sufragios válidamente emitidos. Si ello no sucede, se realiza una segunda vuelta entre las dos primeras mayorías relativas. La regla difiere de la establecida por la Constitución de 1925, que señalaba que en caso de que ningún candidato obtuviere la mayoría absoluta, sería el Senado quien decidiría entre las dos primeras mayorías relativas. La última vez que se aplicó esta regla fue en la elección presidencial de 1970, que llevó a Salvador Allende a la Moneda.  

\subsubsection{Sistema electoral para las elecciones parlamentarias}

\paragraph{El sistema electoral binominal}

En materia de elecciones parlamentarias la cuestión ha sido muy poco pacífica. La Carta de 1980 reemplazó el sistema proporcional que existió hasta 1970, por un sistema bastante sui generis denominado sistema binominal. Este se fundaba en el principio básico de que todas las circunscripciones o distritos, según sea el caso, eligen dos representantes sobre un sistema de listas abiertas\footnote{La diferencia entre distrito y circunscripción es nada más una cuestión de nomenclatura. Se habla de distrito cuando se hace referencia a la elección de Diputados y circunscripciones en la elección de Senadores. Para el caso de la elección de Diputados existían 120 distritos, para la de senadores 19 circunscripciones.}. Cada lista puede presentar a las elecciones un máximo de dos candidatos, cifra que al mismo tiempo expresa el tamaño de la circunscripción. Al momento de traducir los votos en escaños, los votos obtenidos por ambos candidatos se suman a efectos de determinar cuántos cargos corresponden a cada lista. Luego se asignan los escaños al interior de las listas según la votación individual de cada candidato. La lista que alcanza el primer lugar en una circunscripción, únicamente puede obtener ambos escaños, si logra más que el doble del total de sufragios que la lista que obtuvo el segundo lugar. En caso contrario, cada una de las dos listas más votadas consigue sólo un escaño, independientemente de la diferencia de votos que exista entre ellas. 

El sistema binominal fue el tópico más debatido durante toda la transición. En concreto, sólo pudo modificarse luego de más de una decena de intentos. En efecto, contó con férreos partidarios que vieron en él un requisito indispensable para el buen funcionamiento de las instituciones, y en ese sentido, lo consideraban determinante en la estabilidad y gobernabilidad que alcanzó Chile durante la década de 1990. Sin embargo, a nuestro juicio, se trataba de una estabilidad artificialmente producida, y en realidad, no es muy difícil constatar cómo dicho sistema produjo efectos claramente antidemocráticos, como por ejemplo, excluir del Parlamento a todos los partidos políticos que no se integraron en uno de las dos grandes Coaliciones. Por otra parte, existen una serie de estudios que han mostrado con claridad cómo los dos bloques mayoritarios se han visto considerablemente sobrerrepresentados. 

Estos efectos, a primera vista podrían asimilarse a los que típicamente producen los sistemas mayoritarios, sin embargo, en el sistema binominal ocasionaba un curioso efecto que claramente atentaba contra los principios esenciales de la democracia: en la gran mayoría de las ocasiones se producía un empate entre las dos primeras mayorías, es decir, nadie ganaba las elecciones, por lo que algunos autores no tardaron en bautizarlo como ``un seguro para los subcampeones electorales''. Por lo mismo, el sistema binominal fue considerado el verdadero cerrojo que impedía alterar el statu quo de la Constitución de 1980.

\paragraph{El nuevo sistema electoral aplicable a partir de 2017}

Recientemente, la Ley 20.840 de 05 de mayo de 2015, que modifica la Ley N° 18.700 Orgánica Constitucional de Votaciones Populares y escrutinios, vino a sustituir el sistema binominal por un sistema proporcional. Dicho sistema se aplicó por primera vez en las elecciones parlamentarias de 2017. Sin embargo, hay que tener en cuenta que como el período de los Senadores es de 8 años, y según el artículo 49 CPR, éstos se renuevan por parcialidades cada cuatro años, en la propia ley se establece que, para los efectos de completar la nueva integración del Senado, en las elecciones parlamentarias que deben celebrarse en noviembre de 2017 corresponderá que se renueven completamente las circunscripciones que corresponden a Regiones impares. En el caso de las circunscripciones que corresponden a las Regiones pares y Metropolitana, los parlamentarios elegidos en 2013 seguirán en sus funciones hasta completar su período de ocho años. En las elecciones de 2021, estas circunscripciones elegirán al total de los senadores que les corresponde.

El nuevo sistema electoral para las elecciones parlamentarias funciona sobre la base de las siguientes reglas:

\begin{enumerate}[1.]

	\item Se trata de un sistema de circunscripciones plurinominales de tamaño variable. En el caso de la Cámara de Diputados, ésta se compone de 155 miembros, elegidos en 28 distritos de 3, 4, 5, 6, 7 y 8 escaños, respectivamente\footnote{El Consejo Directivo del Servicio Electoral deberá actualizar cada diez años, la asignación de los 155 escaños de Diputados entre los 28 distritos, de acuerdo con último censo oficial de la población realizado por el Instituto Nacional de Estadísticas. No obstante, la ley establece que ningún distrito podrá elegir menos de 3 ni más de 8 Diputados.}.  En el caso del Senado éste se compone de 50 miembros, elegidos en 15 circunscripciones de 2, 3 y 5 escaños, respectivamente.

	\item En cuanto a la estructura de voto, se mantiene un sistema de voto de orden personal sobre la base de listas abiertas.

	\item Respecto a la fórmula electoral, se recurre a una fórmula proporcional del tipo D'Hondt. 

\end{enumerate}

Así las cosas, en nuevo artículo 109 bis del citado cuerpo legal establece que:

En el caso de elecciones de diputados y senadores, el Tribunal Calificador de Elecciones proclamará elegidos a los candidatos, conforme a las reglas establecidas en el procedimiento que a continuación se detalla:

\begin{enumerate}[1.]

	\item El Tribunal Calificador de Elecciones determinará las preferencias emitidas a favor de cada lista y de cada uno de los candidatos que la integran.

	\item Se aplicará el sistema electoral de coeficiente D'Hondt, para lo cual se procederá de la siguiente manera:

	\begin{enumerate}[a)]
		
		\item Los votos de cada lista se dividirán por uno, dos, tres y así sucesivamente hasta la cantidad de cargos que corresponda elegir.

		\item Los números que han resultado de estas divisiones se ordenarán en orden decreciente hasta el número correspondiente a la cantidad de cargos que se eligen en cada distrito electoral o circunscripción senatorial.

		\item A cada lista o pacto electoral se le atribuirán tantos escaños como números tenga en la escala descrita en la letra b).

	\end{enumerate}

\end{enumerate}

Ahora bien, se debe hacer una importante distinción, según se trate de listas conformadas por un solo partido político o se trate de listas que traigan causa en un pacto electoral. Para el primer caso, la regla es simple: el Tribunal Calificador de Elecciones proclamará electos a los candidatos que hayan obtenido las más altas mayorías individuales de cada lista, de acuerdo al número de cargos que le correspondan a cada una de ellas, luego de aplicar las reglas descritas precedentemente. Las reglas para determinar cómo se distribuyen los escaños en el segundo supuesto, responden a la lógica de volver a aplicar la fórmula D'Hondt al interior de la lista. Así, según el numeral 4° del citado artículo 109 bis., el procedimiento para determinar qué partidos dentro de listas heterogéneas obtienen los cargos, es el siguiente:

\begin{enumerate}[a.]

\item Se suman los votos de cada partido político, incluyendo los de las candidaturas independientes asociadas a ese partido. 

\item Se divide por uno, dos, tres y así sucesivamente, hasta la cantidad de cargos obtenidos por el pacto electoral por aplicación de la cifra repartidora. 

\item A cada partido político o, en su caso, a cada partido y las candidaturas independientes asociadas a éste, se le atribuirán tantos escaños como números tenga en la escala descrita en la letra b. precedente.

\item El Tribunal Calificador de Elecciones proclamará elegidos a los candidatos que hayan obtenido las más altas mayorías individuales de cada partido político, incluyendo independientes asociados, de acuerdo a los cupos obtenidos por cada uno de ellos.
Como regla de clausura, cuando exista empate entre candidatos de una misma lista, o entre candidatos de distintas listas que a su vez estén empatadas, el Tribunal Calificador de Elecciones procederá en audiencia pública a efectuar un sorteo entre ellos, y proclamará elegido al que salga favorecido.

\end{enumerate}

Como regla de clausura, cuando exista empate entre candidatos de una misma lista, o entre candidatos de distintas listas que a su vez estén empatadas, el Tribunal Calificador de Elecciones procederá en audiencia pública a efectuar un sorteo entre ellos, y proclamará elegido al que salga favorecido.

