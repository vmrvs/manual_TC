\section{El papel de las declaraciones axiológicas en el Derecho constitucional}

Como gran parte de las Constituciones del mundo, la Carta de 1980 contiene entre sus primeras disposiciones una especie de declaración de principios, en el que se expresa la filosofía que la inspira. En términos históricos, este tipo de disposiciones han sido consideradas por el constitucionalismo comparado como una manifestación del carácter extrajurídico de las normas de la Constitución dogmática, por ende, no vinculantes para los poderes públicos. De este modo, en Europa se entendió hasta mediados del siglo XX que estas cumplían un papel meramente orientador, pero en la actualidad, esa visión ha cambiado y las declaraciones de principios cumplen un importante rol como insumos en la interpretación constitucional.  

Sin embargo, a pesar de que contemporáneamente ya no se cuestione que estas normas cumplan una función plena como parte del ordenamiento constitucional, su aplicación sigue generando debate. Kelsen, por ejemplo, recomendaba no incluir en el texto conceptos como justicia, libertad o igualdad, asimismo, aconsejaba una redacción precisa en todas aquellas cláusulas relativas a derechos fundamentales. En este orden de cosas, este tipo de enunciados ha recibido por la doctrina el nombre de “conceptos esencialmente controvertidos”. Al respecto, Iglesias destaca que estos conceptos poseen cuatro características: a) son conceptos evaluativos, b) son complejos c) tienen un carácter argumentativo, y d) desempeñan una función dialéctica [ CITATION Igl00 \l 13322 ]. En síntesis, se trata de conceptos respecto de las cuales existen distintas visiones alternativas que compiten por dotarlas de contenido, lo que hace dificulta considerablemente su empleo. Si esto es efectivo, entonces, su aplicación práctica requiere un esfuerzo de especificación, o incluso, de desarrollo que está ligado a una postura ideológica concreta

En el caso de la Constitución chilena existe una dificultad adicional, pues en la génesis de la Carta de 1980, confluyen circunstancias históricas que añaden una complejidad distinta a esta materia. Si se observan otras figuras análogas en diferentes Constituciones, el contenido axiológico normalmente está asociado a un compromiso con los valores del Estado de Derecho, lo que en propiedad no sucede en Chile. 

Por ejemplo, el art. 1° de la LFB establece:

\begin{enumerate}[a.]

\item La dignidad humana es intangible. Respetarla y protegerla es obligación de todo poder público.

\item El pueblo alemán, por ello, reconoce los derechos humanos inviolables e inalienables como fundamento de toda comunidad humana, de la paz y de la justicia en el mundo.

\item Los siguientes derechos fundamentales vinculan a los poderes legislativo, ejecutivo y judicial como derecho directamente aplicable

\end{enumerate}

De manera muy parecida, la Constitución francesa de 1958 proclama en su artículo 1°.

\begin{enumerate}[a.]

\item Francia es una República indivisible, laica, democrática y social que garantiza la igualdad ante la ley de todos los ciudadanos sin distinción de origen, raza o religión y que respeta todas las creencias. Su organización es descentralizada. 

\item La ley favorecerá el igual acceso de las mujeres y los hombres a los mandatos electorales y cargos electivos, así como a las responsabilidades profesionales y sociales 

\end{enumerate}

A pesar de que se ha discutido intensamente acerca de los contornos de la noción de Estado de Derecho, la invocación de dichos principios alude a la idea de un sistema constitucional, que expresa un reconocimiento a las distintas concepciones acerca de lo justo como medida de lo que resulta admisible en una sociedad decente. Esta manera de entender los valores constitucionales garantiza cierta coherencia, aunque también ha suscitado críticas debido a su generalidad y ambigüedad. En definitiva, el cuestionamiento que se formula al respecto es que, solo premunidos de estas disposiciones, casi cualquier resultado interpretativo puede ser justificado. A pesar de lo anterior, numerosos tribunales constitucionales del mundo han hecho declaraciones en el sentido de la vinculatoriedad de los valores. 

En este sentido, la complejidad añadida en el caso chileno se debe a que los valores reconocidos por la Constitución de 1980 parecen tener una visión más sesgada que la mirada más amplia que contienen los principios del Estado de Derecho consagrados en otras Constituciones. Pero incluso, si lo aún lo anterior no fuera suficiente dificultad, no siempre es fácil observar coherencia ideológica en las declaraciones axiológicas del capítulo primero. En este punto, podemos citar el artículo de Bassa y Viera, que muestra que en la Constitución chilena no es fácil encontrar coherencia en esta materia, sino más bien es posible advertir un sincretismo teórico de carácter ideológico. En efecto, no es posible explicar su contenido a partir de una sola ideología, sino más bien existe una diversidad de doctrinas que la informan (ius-naturalismo, anarcoliberalismo, democracia instrumental), que incluso en ocasiones entran en tensión entre ellas. \cite{Bas08}. Todo esto muestra lo incorrecto que resulta interpretar la Constitución a partir de un enfoque originalista, a pesar de la insistencia en ello de la jurisprudencia y parte importante de la doctrina.

Sin perjuicio de todo lo anterior, nuestro Tribunal Constitucional se ha pronunciado en similar sentido que la jurisprudencia comparada, reivindicando el papel de las declaraciones axiológicas, al declarar que: “Los principios y valores establecidos en la Constitución no configuran meras declaraciones programáticas, sino que constituyen mandatos expresos para gobernantes y gobernados, dada la fuerza obligatoria de los preceptos constitucionales en virtud de lo dispuesto en el artículo 6º. (STC 46 c. 21, STC 280 c. 12, STC 1185 cons. 11 y 12, STC 2410 cons. 11 y 12, STC 2747 c. 12, STC 2801 c. 12, STC 2860 c. 14, STC 2887 c. 19)”. 

Como sea, en lo que sigue se analizará el contenido del artículo 1° CPR, el que se cita a continuación, para inmediatamente comentar sus disposiciones en detalle:

\begin{quote}

``Artículo 1.- Las personas nacen libres e iguales en dignidad y derechos.
 
La familia es el núcleo fundamental de la sociedad.

El Estado reconoce y ampara a los grupos intermedios a través de los cuales se organiza y estructura la sociedad y les garantiza la adecuada autonomía para cumplir sus propios fines específicos.

El Estado está al servicio de la persona humana y su finalidad es promover el bien común, para lo cual debe contribuir a crear las condiciones sociales que permitan a todos y a cada uno de los integrantes de la comunidad nacional su mayor realización espiritual y material posible, con pleno respeto a los derechos y garantías que esta Constitución establece.

Es deber del Estado resguardar la seguridad nacional, dar protección a la población y a la familia, propender al fortalecimiento de ésta, promover la integración armónica de todos los sectores de la Nación y asegurar el derecho de las personas a participar con igualdad de oportunidades en la vida nacional''.

\end{quote}
